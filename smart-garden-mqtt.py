import time
import datetime
import json
import logging
import os

import pytz
import requests
import schedule
import RPi.GPIO as GPIO
from influxdb import InfluxDBClient
from pushbullet import Pushbullet
import paho.mqtt.client as mqtt

def defineGlobalVariables():

    log_path = os.environ['APP_DATA']
    LOG_FORMAT = '%(asctime)s - %(levelname)s - %(message)s'
    logging.basicConfig(format=LOG_FORMAT, filename= log_path + '/weather-station-rpi-manager.log', level=logging.INFO)
    global log
    log = logging.getLogger()

    try:
        global influx_ip
        global influx_port
        global influx_user
        global influx_password
        global influx_database
        global influx_url

        global mqtt_ip
        global mqtt_port
        
        global tz
        global pushbullet_api_key
        
        global sensor_data_in_topic
        global sensor_data_out_topic
        global sensor_status_in_topic
        global sensor_status_out_topic
        global sensor_sleep_in_topic
        
        global wait_time_error
        global number_attempts
        global relay_pin
        global first_hour
        global second_hour
        global watering_time
        global sleep_time
        global moisture_threshold
        global environment
        global sensorOnline

        sensorOnline = False
        influx_ip = os.environ['INFLUX_IP']
        influx_port = os.environ['INFLUX_PORT']
        influx_user = os.environ['INFLUX_USER']
        influx_password = os.environ['INFLUX_PASSWORD']
        influx_database = os.environ['INFLUX_DATABASE']
        influx_url = 'http://' + influx_ip + ':' + influx_port

        mqtt_ip = os.environ['MQTT_IP']
        mqtt_port = os.environ['MQTT_PORT']

        tz = pytz.timezone(os.environ['TZ'])
        pushbullet_api_key = os.environ['PUSHBULLET_API_KEY']
        
        sensor_data_in_topic = '/wemos/in/sensors'
        sensor_data_out_topic = '/wemos/out/sensors'
        sensor_status_in_topic = '/wemos/in/status'
        sensor_status_out_topic = '/wemos/out/status'
        sensor_sleep_in_topic = '/wemos/in/sleep'
        
        wait_time_error = int(os.environ['WAIT_TIME_ERROR'])
        number_attempts = int(os.environ['NUMBER_ATTEMPTS'])
        relay_pin = int(os.environ['RELAY_PIN'])
        first_hour = os.environ['FIRST_HOUR']
        second_hour = os.environ['SECOND_HOUR']
        watering_time = int(os.environ['WATERING_TIME'])
        sleep_time = int(os.environ['SLEEP_TIME'])
        moisture_threshold = int(os.environ['MOISTURE_THRESHOLD'])
        environment = os.environ['ENV'].lower()

    except KeyError as err:
        logMessage('The following key could not be found on the environment variables: ' + err, logging.ERROR)

def on_connect(client, userdata, flags, rc):
    if rc == 0:
        client.connected_flag=True
        logMessage("Connected on MQTT server with result code "+str(rc), logging.INFO)
        logMessage("Subscribing topics ...", logging.INFO)
        client.subscribe([sensor_data_out_topic, sensor_status_out_topic])
        logMessage("Subscribed.", logging.INFO)
    else:
        logMessage("Bad connection Returned code= " + rc, logging.ERROR)

def on_disconnect(client, userdata,rc=0):
    logMessage("DisConnected MQTT server with result code "+str(rc), logging.INFO)
    client.connected_flag=False
    client.loop_stop()


def handle_sensor_status_response(message):
    if message != '' and not is_between_allowed_period():
        nextRun = int(schedule.idle_seconds()) - 5
        hibernate_sensor(nextRun)


def handle_sensor_data_response(payload):
    sensor_data = json.loads(payload)
    try:
        hibernate_sensor(sleep_time) #seconds

        #TODO treat possible exceptions here
        persist(sensor_data)
        #buscar previsão do tempo
        processSensorData(sensor_data)
        time_delta = datetime.timedelta(hours=1)
        logMessage('Next run in: ' + str(schedule.next_run() + time_delta), logging.INFO)
    except requests.exceptions.RequestException as err:
        message = 'Unable to connect on sensor. I will try again on the next job in %d seconds'
        logMessage(message % sleep_time, logging.INFO)
        push.push_note('ERROR', message % sleep_time)
        logMessage(err, logging.ERROR)


def on_message(client, userdata, message):
    if message.topic == sensor_data_out_topic :
        handle_sensor_data_response(str(message.payload.decode("utf-8")))
        return

    if message.topic == sensor_status_out_topic :
        handle_sensor_status_response(message)

def setup():

    defineGlobalVariables()

    all_good = True

    if environment != 'dev':
        logMessage('Config pins', logging.INFO)
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(relay_pin, GPIO.OUT)
        GPIO.output(relay_pin, GPIO.HIGH)

    logMessage('Connecting Pushbullet ...', logging.INFO)
    global push
    push = Pushbullet(pushbullet_api_key)

    logMessage('Connecting influxdb ...', logging.INFO)
    for i in range(0, number_attempts):
        try:
            response = requests.get(influx_url + '/status')
            HTTP_OK_BUT_NOT_CONTENT=204
            if(response.status_code == HTTP_OK_BUT_NOT_CONTENT):
                global influx_client
                influx_client = InfluxDBClient(influx_ip, influx_port, influx_user, influx_password, influx_database)
                logMessage('InfluxDB online.', logging.INFO)
                break
        except requests.exceptions.ConnectionError:
            if i == number_attempts:
                message = 'Unable to connect on influxdb.'
                logMessage(message, logging.ERROR)
                push.push_note('ERROR', message)
                all_good = False
            else:
                logMessage('InfluxDB have some trouble, maybe was not started yet. I will wait some time...', logging.ERROR)
                time.sleep(wait_time_error)

    logMessage('Connecting MQTT server ...', logging.INFO)
    global mqtt_client
    mqtt.Client.connected_flag=False
    mqtt_client = mqtt.Client("RPI")
    mqtt_client.on_connect = on_connect
    mqtt_client.on_disconnect = on_disconnect
    mqtt_client.on_message = on_message
    mqtt_client.connect(mqtt_ip, mqtt_port)
    mqtt_client.loop_start()

    for i in range(0, number_attempts):
        if not mqtt_client.connected_flag:
            if i == number_attempts:
                message = 'Unable to connect on MQTT server.'
                logMessage(message, logging.ERROR)
                push.push_note('ERROR', message)
                all_good = mqtt_client.connected_flag
            else:
                logMessage('Trying to connect in MQTT. Attempt #' + i, logging.INFO)
                time.sleep(wait_time_error)
                continue

        break


    # logMessage('Connecting sensor ...', logging.INFO)
    # for i in range(0, number_attempts):
    #     logMessage('Attempt #' + str(i), logging.INFO)
    #     try:
    #         sensorResponse = requests.get(sensor_url + '/status')
    #         HTTP_OK = 200
    #         if(sensorResponse.status_code == HTTP_OK):
    #             logMessage('Sensors online.', logging.INFO)
    #             global sensorOnline
    #             sensorOnline = True
    #             break
    #     except requests.exceptions.ConnectionError:
    #         if i == number_attempts:
    #             message = 'Unable to connect on the sensor.'
    #             logMessage(message, logging.ERROR)
    #             push.push_note('ERROR', message)
    #         else:
    #             message = 'Sensor have some trouble, I will try again in %d seconds'
    #             logMessage(message %(wait_time_error), logging.ERROR)
    #             time.sleep(wait_time_error)

    if not all_good:
        destroy()

def castIntegerFieldsToFloat(jsonData):
    jsonData["temperature"] = float(jsonData["temperature"])
    jsonData["pressure"] = float(jsonData["pressure"])
    jsonData["altitude"] = float(jsonData["altitude"])
    jsonData["batteryVoltage"] = float(jsonData["batteryVoltage"])
    jsonData["solarVoltage"] = float(jsonData["solarVoltage"])

def persist(jsonData):
    logMessage('Persisting json data to database', logging.INFO)
    castIntegerFieldsToFloat(jsonData)
    json_body = [
            {"measurement": "weather_station",
             "tags": {"sensor" : "wemos"},
             "time": datetime.datetime.now(tz).strftime('%Y-%m-%d %H:%M:%S'),
             "fields": jsonData
            }
    ]

    influx_client.write_points(json_body)

def irrigate(sensorData):
    if environment != 'dev':
        GPIO.output(relay_pin, GPIO.LOW)
    logMessage('Solenoid is on', logging.INFO)
    push.push_note('Solenoid is on', json.dumps(sensorData, indent=4, sort_keys=True))

    time.sleep(watering_time)

    if environment != 'dev':
        GPIO.output(relay_pin, GPIO.HIGH)
    logMessage('Solenoid is off', logging.INFO)
    push.push_note('Solenoid is off', datetime.datetime.now(tz).strftime('%Y-%m-%d %H:%M:%S'))

def destroy():
    GPIO.output(relay_pin, GPIO.HIGH)
    logMessage('Cleanning up pins', logging.INFO)
    GPIO.cleanup()

    logMessage('Disconnecting MQTT broker', logging.INFO)
    mqtt_client.disconnect()
    exit()

def getSensorData():
    logMessage('Requesting sensor data ...', logging.INFO)

    # response = requests.get(sensor_url+"/getSensors")
    # dataJson = json.loads(response.text)
    # logMessage(dataJson, logging.INFO)
    # return dataJson

def hibernate_sensor(seconds):
    logMessage('Putting sensor to sleep for ' + str(seconds) + ' seconds', logging.INFO)
    mqtt_client.publish(sensor_sleep_in_topic, seconds, 1)

def processSensorData(sensorData):
    if sensorData['moisture'] < moisture_threshold:
        irrigate(sensorData)

def job():
    if is_between_allowed_period():
        logMessage('Start main job at: ' + str(schedule.next_run()), logging.INFO)
        #buscar da base quantas irrigações foram feitas.

        getSensorData()

def is_between_allowed_period():
    hour = datetime.datetime.now(tz).strftime('%H')
    return first_hour <= hour <= second_hour

def logMessage(message, level):
    if environment == 'dev':
        print(message)
        return
    
    if level == logging.ERROR:
        log.error(message)
    else:
        log.info(message)

if __name__ == '__main__':
    
    setup()

    schedule.every().hour.at(':00').do(job)
    logMessage(schedule.jobs, logging.INFO)
    logMessage('Setting first irrigation time to %d and the second to %d ' % (first_hour, second_hour), logging.INFO)

    # if sensorOnline:
    #     nextRun = int(schedule.idle_seconds()) - 5
    #     hibernate_sensor(nextRun)

    try:
        while True:
            schedule.run_pending()
            time.sleep(1)

    except KeyboardInterrupt:
        destroy()
    finally:
        destroy()
