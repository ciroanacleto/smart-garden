import datetime
import json
import logging
import os
import time

import RPi.GPIO as GPIO
import pytz
import requests
import schedule
import telegram
from influxdb import InfluxDBClient
from pushbullet import Pushbullet


def define_global_variables():
    log_path = os.environ['APP_DATA']
    log_format = '%(asctime)s - %(levelname)s - %(message)s'
    logging.basicConfig(format=log_format, filename=log_path + '/smart-garden-http.log', level=logging.INFO)
    global log
    log = logging.getLogger()

    try:
        global influx_ip
        global influx_port
        global influx_user
        global influx_password
        global influx_database
        global influx_url
        global tz
        global pushbullet_api_key
        global telegram_api_key
        global telegram_chat_id
        global logging_channel
        global sensor_ip
        global sensor_url
        global wait_time_error
        global number_attempts
        global relay_pin
        global first_hour
        global second_hour
        global watering_time
        global sleep_time
        global moisture_threshold
        global environment
        global sensor_online

        sensor_online = False
        environment = os.environ['ENV'].lower()
        influx_ip = os.environ['INFLUX_IP']
        influx_port = os.environ['INFLUX_PORT']
        influx_user = os.environ['INFLUX_USER']
        influx_password = os.environ['INFLUX_PASSWORD']
        influx_database = os.environ['INFLUX_DATABASE']
        influx_url = 'http://' + influx_ip + ':' + influx_port
        tz = pytz.timezone(os.environ['TZ'])
        pushbullet_api_key = os.environ['PUSHBULLET_API_KEY']
        telegram_api_key = os.environ['TELEGRAM_API_KEY']
        telegram_chat_id = os.environ['TELEGRAM_CHAT_ID']
        logging_channel = os.environ['LOGGING_CHANNEL']
        sensor_ip = os.environ['SENSOR_IP']
        sensor_url = 'http://' + sensor_ip
        wait_time_error = int(os.environ['WAIT_TIME_ERROR'])
        number_attempts = int(os.environ['NUMBER_ATTEMPTS'])
        relay_pin = int(os.environ['RELAY_PIN'])
        first_hour = os.environ['FIRST_HOUR']
        second_hour = os.environ['SECOND_HOUR']
        watering_time = int(os.environ['WATERING_TIME'])
        sleep_time = int(os.environ['SLEEP_TIME'])
        moisture_threshold = int(os.environ['MOISTURE_THRESHOLD'])

    except KeyError as err:
        log_message('The following key could not be found on the environment variables: ' + str(err), logging.ERROR)


def setup():
    define_global_variables()

    if environment != 'dev':
        log_message('Config pins', logging.INFO)
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(relay_pin, GPIO.OUT)
        GPIO.output(relay_pin, GPIO.HIGH)

    global channel
    if logging_channel == 'pushbullet':
        channel = Pushbullet(pushbullet_api_key)
    else:
        channel = telegram.Bot(token=telegram_api_key)

    log_message('System started.', logging.INFO)
    send_message(logging.getLevelName(logging.INFO) + ' - ' + 'System started.')

    all_good = test_influxdb_connection()

    test_sensor_connection()

    if not all_good:
        destroy()


def destroy():
    cleanup_pins()
    message = 'System is shutting down, good bye!'
    log_message(message, logging.INFO)
    send_message(logging.getLevelName(logging.INFO) + ' - ' + message)
    exit()


def test_sensor_connection():
    log_message('Connecting sensor ...', logging.INFO)
    for i in range(1, number_attempts + 1):
        log_message('Attempt #' + str(i), logging.INFO)
        try:
            sensor_response = requests.get(sensor_url + '/status')
            http_ok = 200
            if sensor_response.status_code == http_ok:
                log_message('Sensors online.', logging.INFO)
                global sensor_online
                sensor_online = True
                break
        except requests.exceptions.ConnectionError:
            if i == number_attempts:
                message = 'Unable to connect on the sensor.'
                log_message(message, logging.ERROR)
                send_message(logging.getLevelName(logging.ERROR) + ' - ' + message)
                break
            else:
                message = 'Sensor have some trouble, I will try again in %d seconds'
                log_message(message % wait_time_error, logging.ERROR)
                send_message(logging.getLevelName(logging.ERROR) + ' - ' + message % wait_time_error)
                time.sleep(wait_time_error)


def test_influxdb_connection():
    log_message('Connecting influxdb ...', logging.INFO)
    for i in range(1, number_attempts + 1):
        log_message('Attempt #' + str(i), logging.INFO)
        try:
            response = requests.get(influx_url + '/status')
            http_ok_but_not_content = 204
            if response.status_code == http_ok_but_not_content:
                global influx_client
                influx_client = InfluxDBClient(influx_ip, influx_port, influx_user, influx_password, influx_database)
                log_message('InfluxDB online.', logging.INFO)
                return True
        except requests.exceptions.ConnectionError:
            if i == number_attempts:
                message = 'Unable to connect on influxdb.'
                log_message(message, logging.ERROR)
                send_message(message)
                return False
            else:
                log_message('InfluxDB have some trouble, maybe was not started yet. I will wait some time...', logging.ERROR)
                time.sleep(wait_time_error)


def cast_integer_fields_to_float(json_data):
    json_data["temperature"] = float(json_data["temperature"])
    json_data["pressure"] = float(json_data["pressure"])
    json_data["altitude"] = float(json_data["altitude"])
    json_data["batteryVoltage"] = float(json_data["batteryVoltage"])
    json_data["solarVoltage"] = float(json_data["solarVoltage"])


def persist(json_data):
    log_message('Persisting json data to database', logging.INFO)
    cast_integer_fields_to_float(json_data)
    json_body = [
        {"measurement": "weather_station",
         "tags": {"sensor": "wemos"},
         "time": datetime.datetime.now(tz).isoformat(),
         "fields": json_data
         }
    ]

    influx_client.write_points(json_body)


def irrigate(sensor_data):
    if environment != 'dev':
        GPIO.output(relay_pin, GPIO.LOW)
    log_message('Solenoid is on', logging.INFO)
    send_message(json.dumps(sensor_data, indent=4, sort_keys=True))

    time.sleep(watering_time)

    if environment != 'dev':
        GPIO.output(relay_pin, GPIO.HIGH)
    log_message('Solenoid is off', logging.INFO)
    send_message(datetime.datetime.now(tz).strftime('%Y-%m-%d %H:%M:%S'))


def cleanup_pins():
    if environment != 'dev':
        GPIO.output(relay_pin, GPIO.HIGH)
        log_message('Cleanning up pins', logging.INFO)
        GPIO.cleanup()


def get_sensor_data():
    log_message('Requesting sensor data ...', logging.INFO)
    response = requests.get(sensor_url + "/getSensors", timeout=wait_time_error)
    return json.loads(response.text)


def hibernate_sensor(seconds):
    try:
        log_message('Putting sensor to sleep for ' + str(seconds) + ' seconds', logging.INFO)
        requests.get(sensor_url + "/sleep?time=" + str(seconds))
    except requests.exceptions.RequestException as err:
        log_message(err, logging.ERROR)


def process_sensor_data(sensor_data):
    if 0 < int(sensor_data['moisture']) < moisture_threshold:
        irrigate(sensor_data)


def job():
    hour = datetime.datetime.now(tz).strftime('%H')
    if first_hour <= hour <= second_hour:
        log_message('Start main job at: ' + str(schedule.next_run()), logging.INFO)

        try:
            sensor_data = get_sensor_data()
            hibernate_sensor(sleep_time)  # seconds

            # TODO treat possible exceptions here
            persist(sensor_data)
            # search weather data from server darksky
            process_sensor_data(sensor_data)
            time_delta = datetime.timedelta(hours=1)
            log_message('Next run in: ' + str(schedule.next_run() + time_delta), logging.INFO)
        except Exception as err:
            cleanup_pins()
            message = 'Unable to connect on sensor. I will try again on the next job in %d seconds'
            log_message(message % sleep_time, logging.ERROR)
            log_message(err, logging.ERROR)

            send_message(message % sleep_time)
            send_message(err)
    else:
        log_message('Nightly job. Should sleep for %d seconds.' % sleep_time, logging.INFO)
        hibernate_sensor(sleep_time)


def log_message(message, level):
    if environment == 'dev':
        print(message)

    if level == logging.ERROR:
        log.error(message)
    else:
        log.info(message)


def send_message(message):
    try:
        if logging_channel == 'pushbullet':
            channel.push_note('', message)
        else:
            channel.sendMessage(chat_id=telegram_chat_id, text=message)
    except Exception as err:
        log_message('Fail to send message to configured channel %s' % logging_channel, logging.ERROR)
        log_message(err)


if __name__ == '__main__':

    setup()

    schedule.every().hour.at(':00').do(job)
    log_message(schedule.jobs, logging.INFO)
    log_message('Setting first irrigation time to %s and the second to %s ' % (first_hour, second_hour), logging.INFO)

    if sensor_online:
        nextRun = int(schedule.idle_seconds()) - 5
        hibernate_sensor(nextRun)

    try:
        while True:
            schedule.run_pending()
            time.sleep(1)
    finally:
        destroy()

