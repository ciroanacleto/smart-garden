# Python Base Image from https://hub.docker.com/r/arm32v7/python/
FROM arm32v7/python:3.6.8-jessie

ENV APP_PATH="/opt/smart_garden"
ENV APP_DATA="/var/smart_garden"
ENV TZ="America/Araguaina"

WORKDIR $APP_PATH

# Intall the rpi.gpio python module
RUN mkdir -p $APP_PATH \
    && mkdir -p $APP_DATA \
    && ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone \
    && pip install --upgrade pip \
    && pip install --no-cache-dir rpi.gpio pytz requests schedule influxdb pushbullet.py paho-mqtt python-telegram-bot

# Copy the Python Script
COPY ["smart-garden-http.py", "$APP_PATH/smart-garden-http.py"]
#COPY ["smart-garden-mqtt.py", "$APP_PATH/smart-garden-mqtt.py"]

EXPOSE 5676

# Trigger Python script
CMD ["python", "/opt/smart_garden/smart-garden-http.py"]
#CMD ["python", "/opt/smart_garden/smart-garden-mqtt.py"]
