# Smart Garden IoT

This project is responsible to read data from my weather station sensors and irrigate my kitchen garden under specific conditions.

This project itself doesn't work without the other project "weather-station-sensors" once is needed to read the data from sensors like soil moisture.

The smart garden project was designed to run in Raspberry Pi 3 B+. It was used a relay module plugged on GPIO specified on _docker-compose.yml_ file in `RELAY_PIN` parameter. A 220V solenoid valve is used to control the water.

To run this project is required, besides the weather-station-sensor project, to edit the _docker-compose.yml_ file, the parameters bellow:

```
INFLUX_IP = influx database ip
INFLUX_PORT = influx database port
INFLUX_USER = influx database user
INFLUX_PASSWORD = influx database password
INFLUX_DATABASE = influx database name
PUSHBULLET_API_KEY = pushbullet API key to get notifications on smartphone
SENSOR_IP = weather-station-sensor ip
WAIT_TIME_ERROR = time in seconds between attempts
NUMBER_ATTEMPTS = integer number of attempts that must to be tryed in case of sensor have some problem
RELAY_PIN = GPIO pin that relay module is pluged
FIRST_HOUR = formatted (hh:mm) hour to beggin the job
SECOND_HOUR = formatted (hh:mm) hour to stop the job
WATERING_TIME = time in seconds that the solenoid valve must to stay opened
SLEEP_TIME = time in seconds to sleep the sensors
MOISTURE_THRESHOLD = minimum limit that is acceptable moisture
TELEGRAM_API_KEY= API key from telegram chat bot
TELEGRAM_CHAT_ID= Chat id to send messages
LOGGING_CHANNEL='telegram' or 'pushbullet' ; specifyes where the notification messages must to be sended
ENV = 'dev' or 'prod' ; specifyes if the system must to run in debug mode (without open the water) or to run in "production mode" (working with irrigation)
```
A internet connection in Raspberry Pi is required because the docker image and python dependencies it will be downloaded.

After all modifications, to run the container just cd into project directory, open a terminal and type:

`docker-compose up -d`

It will be created a log file in `~/docker/smart-garden`. Type on terminal :

```tail -f ~/docker/smart-garden.log```

If everything is ok, on log will be shownd the time for the next job.
